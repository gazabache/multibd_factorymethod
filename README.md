# MultiBaseDatos - Factory Method Design Pattern

Functional project in .NET Core that implements FACTORY METHOD DESIGN PATTERN for making a web app that uses several DB engines (SQL Server and MySql) using development good practices.

Proyecto funcional con .NET Core que implementa el patrón de diseño FACTORY METHOD para hacer una aplicación web que utiliza varios motores de bases de datos (SQL Server y MySQL) con buenas prácticas.

I hope this project helps you in the understanding of design patterns, in this case: Factory method. 

## Prerequisites

-BD in SQL Server and MySQL with sample data
-Set the database configuration properly (more info in the notes section)

For achieving this, execute this SQL Scripts in each dbms:
```/* ====== SQL SERVER ====== */
/*Creating database*/
CREATE DATABASE multibd;
GO
/*Creating table*/
USE multibd;
CREATE TABLE Producto
(
	idProducto int primary key identity(1,1),
	Nombre varchar(50),
	Precio decimal(10,2)
);
/*Inserting sample data*/
INSERT INTO Producto (Nombre, Precio) VALUES('Sql product 1',4.3)


/* ====== MySQL ====== */
/*Creating database*/
CREATE DATABASE multibd;

/*Creating table*/
USE multibd;
CREATE TABLE Producto
(
	idProducto int primary key auto_increment,
	Nombre varchar(50),
	Precio decimal(10,2)
);
/*Inserting sample data*/
INSERT INTO Producto(Nombre,Precio) VALUES('MySQL product 1',5.5)
```

## Notes

- In the project UI > Meta-Inf there are 2 property files to set up the db conections
- In the project UI > Edit the appsettings.json to set the default dbms with the DefaultDBClass node


Gerson.
